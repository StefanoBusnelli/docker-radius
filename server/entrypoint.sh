#!/bin/bash
if [[ -z ${1} ]]; then
  exec freeradius -X > /var/log/freeradius/debug.log 
else
  exec $@
fi
