#!/bin/bash

docker volume create radius_etc
docker volume create radius_log

rm radius
rm radius_etc
rm radius_log
ln -s /var/lib/docker/volumes/radius_etc/_data/ radius_etc
ln -s /var/lib/docker/volumes/radius_log/_data/ radius_log

docker run -d --rm \
           --name=radius \
	   --publish 1812-1814:1812-1814/udp --publish 18120:18120/udp \
	   --mount source=radius_etc,destination=/etc/freeradius \
	   --mount source=radius_log,destination=/var/log/freeradius \
	   busnellistefano/radius:latest

