#!/bin/bash
LOC_DIR=$(pwd)
INSTALL_DIR=/opt/radius

if [[ -e ./bin/build.sh ]]; then
  printf "\033[1;32m Build images ... \033[0m\r\n"
  . ./bin/build.sh
else
  printf "\033[1;32m Download images ... \033[0m\r\n"
  docker pull busnellistefano/radius_client:latest
  docker pull busnellistefano/radius:latest
fi

printf "\033[1;32m Installing scripts in $INSTALL_DIR/ ... \033[0m\r\n"

mkdir -p $INSTALL_DIR/bin
mkdir -p $INSTALL_DIR/etc

cd $LOC_DIR
cp bin/compose.sh $INSTALL_DIR/bin/
cp bin/start.sh $INSTALL_DIR/bin/
cp bin/stop.sh $INSTALL_DIR/bin/
cp bin/restart.sh $INSTALL_DIR/bin/
cp bin/update_services.sh $INSTALL_DIR/bin/
chmod +x $INSTALL_DIR/bin/*

cp etc/*.yml $INSTALL_DIR/etc/

printf "\033[1;32m Linking mounts ... \033[0m\r\n"

cd $INSTALL_DIR
rm radius_etc
rm radius_log
ln -s /var/lib/docker/volumes/radius_etc/_data/ radius_etc
ln -s /var/lib/docker/volumes/radius_log/_data/ radius_log

printf "\033[1;32m End \033[0m\r\n"
cd $LOC_DIR
