#!/bin/bash
printf "\033[1;32m"
echo "*************************"
echo "* Build radius client   *"
echo "*************************"
printf "\033[0m"
docker build -t busnellistefano/radius_client:latest .
docker push busnellistefano/radius_client:latest
