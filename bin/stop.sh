#!/bin/bash
LOC_DIR=$(pwd)
INSTALL_DIR=/opt/radius

cd $INSTALL_DIR
docker service scale radius_server=0
cd $LOC_DIR
