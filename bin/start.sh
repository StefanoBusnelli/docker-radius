#!/bin/bash
LOC_DIR=$(pwd)
INSTALL_DIR=/opt/radius

cd $INSTALL_DIR
./bin/compose.sh
docker service scale radius_server=1
cd $LOC_DIR
