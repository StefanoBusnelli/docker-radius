# Radius server #

This is a RADIUS server running in docker.

## Get images ##

```
docker pull busnellistefano/radius_client:latest
docker pull busnellistefano/radius:latest 
```

## Build images ##

```
git clone git@bitbucket.org:StefanoBusnelli/docker-radius.git
cd docker-radius
./bin/build.sh
```

## Install ##

```
./install.sh
```

## Note ##

Images are build for armv7l ( raspberry PI 3b )
For other architectures, edit the Dockerfile in the client folder, change the base image at the top of the file from raspbian to alpine, debian, ubuntu or whatever you want and rebuild. The server image is based upon the client.

